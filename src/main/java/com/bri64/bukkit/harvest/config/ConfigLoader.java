package com.bri64.bukkit.harvest.config;

import com.bri64.bukkit.harvest.SimpleHarvest;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

public class ConfigLoader {

  private SimpleHarvest plugin;
  private Map<Integer, Integer> chances;
  private int defaultChance;

  public ConfigLoader(final SimpleHarvest plugin, final FileConfiguration config) {
    this.plugin = plugin;
    this.chances = new HashMap<>();
    this.defaultChance = 50;
    loadConfig(config);
  }

  public int getChance(int multiplier) {
    if (chances.containsKey(multiplier)) {
      return chances.get(multiplier);
    }
    return defaultChance;
  }

  private void loadConfig(FileConfiguration config) {
    if (config.contains("chances", true)) {
      ConfigurationSection ch = config.getConfigurationSection("chances");
      for (String key : ch.getKeys(false)) {
        if (key.equals("default")) {
          int chance = ch.getInt(key);
          if (chance > 0 && chance <= 100) {
            defaultChance = chance;
          } else {
            plugin.getLog().warning("Config Error: \"" + key + "\" has invalid chance!");
          }
        } else {
          try {
            int multiplier = Integer.parseInt(key);
            int chance = ch.getInt(key);
            if (chance > 0 && chance <= 100) {
              chances.put(multiplier, chance);
            } else {
              plugin.getLog().warning("Config Error: \"" + key + "\" has invalid chance!");
            }
          } catch (NumberFormatException ex) {
            plugin.getLog().warning("Config Error: \"" + key + "\" is not a valid number!");
          }
        }
      }
    } else {
      plugin.getLog().warning("Config Error: missing \"chances\" section!");
    }
  }
}
