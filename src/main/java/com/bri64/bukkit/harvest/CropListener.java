package com.bri64.bukkit.harvest;

import com.bri64.bukkit.harvest.config.ConfigLoader;
import com.bri64.bukkit.harvest.crops.Crop;
import com.bri64.bukkit.harvest.crops.CropFactory;
import com.bri64.bukkit.harvest.crops.NetherCrop;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import java.util.Random;
import org.bukkit.GameMode;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

@SuppressWarnings({"WeakerAccess", "unused", "FieldCanBeLocal"})
public class CropListener implements Listener {

  private SimpleHarvest plugin;
  private ConfigLoader config;

  public CropListener(final SimpleHarvest plugin, final ConfigLoader config) {
    this.plugin = plugin;
    this.config = config;
  }

  @EventHandler
  public void onBreakBlock(BlockBreakEvent event) {
    Player player = event.getPlayer();
    Block block = event.getBlock();
    if (CropFactory.isSupported(block)) {
      if (plugin.hasWorldGuard()) {
        WorldGuardPlugin worldGuard = plugin.getWorldGuard();
        if (!worldGuard.canBuild(player, block)) {
          return;
        }
      }

      if (CropUtils.getMultiplierPerm(player) != null) {
        Crop crop = CropFactory.createCrop(block);
        if (crop == null || !crop.isLeftClickable()) {
          return;
        }
        breakBlock(player, crop);
        event.setCancelled(true);
      }
    }
  }

  @EventHandler
  public void onRightClickBlock(PlayerInteractEvent event) {
    if (isRightClick(event)) {
      Player player = event.getPlayer();
      Block block = event.getClickedBlock();

      if (CropFactory.isSupported(block)) {
        if (plugin.hasWorldGuard()) {
          WorldGuardPlugin worldGuard = plugin.getWorldGuard();
          if (!worldGuard.canBuild(player, block)) {
            return;
          }
        }

        if (player.hasPermission(CropPermissions.USE.toString())) {
          Crop crop = CropFactory.createCrop(block);
          if (crop == null || !crop.isRightClickable()) {
            return;
          }
          breakBlock(player, crop);
          return;
        }
        if (player.hasPermission(CropPermissions.BONE.toString())) {
          Crop crop = CropFactory.createCrop(block);
          if (!(crop instanceof NetherCrop) || !CropUtils.holdingBone(event.getItem())) {
            return;
          }
          growCrop(player, crop, event.getItem());
          // return;
        }
      }
    }
  }

  private boolean isRightClick(PlayerInteractEvent event) {
    return event.getAction() == Action.RIGHT_CLICK_BLOCK
        && event.getHand() == EquipmentSlot.HAND;
  }

  @SuppressWarnings("UnusedReturnValue")
  private boolean breakBlock(Player player, Crop crop) {
    if (crop.isRipe()) {
      int[] multi_chance = CropUtils.getMultiplier(player, config);
      boolean doMulti = (new Random().nextInt(100) + 1) <= multi_chance[1];
      int amt = doMulti ? multi_chance[0] : 1;
      crop.doBreak(player, amt);
      if (amt != 1) {
        crop.animateHarvest();
      }
      return true;
    }
    return false;
  }

  @SuppressWarnings("UnusedReturnValue")
  private boolean growCrop(Player player, Crop crop, ItemStack hand) {
    if (!crop.isRipe()) {
      if (player.getGameMode() != GameMode.CREATIVE) {
        hand.setAmount(hand.getAmount() - 1);
      }
      crop.grow();
      crop.animateGrowth();
      crop.update();
      return true;
    }
    return false;
  }
}
