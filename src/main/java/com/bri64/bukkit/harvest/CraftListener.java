package com.bri64.bukkit.harvest;

import com.bri64.bukkit.harvest.config.ConfigLoader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.FurnaceInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

@SuppressWarnings({"WeakerAccess", "unused", "FieldCanBeLocal"})
public class CraftListener implements Listener {

  private static Set<ItemStack> SUPPORTED_RECIPES;
  private static Set<ItemStack> SUPPORTED_SMELTS;

  static {
    SUPPORTED_RECIPES = new HashSet<>();
    SUPPORTED_RECIPES.add(new ItemStack(Material.SUGAR));
    SUPPORTED_RECIPES.add(new ItemStack(Material.PAPER));
    //SUPPORTED_RECIPES.add(new ItemStack(Material.PUMPKIN_SEEDS));
    //SUPPORTED_RECIPES.add(new ItemStack(Material.MELON_SEEDS));

    SUPPORTED_SMELTS = new HashSet<>();
    SUPPORTED_SMELTS.add(CropUtils.createStack(Material.INK_SACK, (short) 2));
    SUPPORTED_SMELTS.add(new ItemStack(Material.CHORUS_FRUIT_POPPED));
  }

  private SimpleHarvest plugin;
  private ConfigLoader config;

  public CraftListener(final SimpleHarvest plugin, final ConfigLoader config) {
    this.plugin = plugin;
    this.config = config;
  }

  @EventHandler
  public void onCraft(CraftItemEvent event) {
    CraftingInventory craftInventory = event.getInventory();
    ItemStack result = craftInventory.getResult();
    HumanEntity player = event.getWhoClicked();
    PlayerInventory playerInventory = player.getInventory();
    if (result == null || !(player instanceof Player)) {
      return;
    }

    if (CropUtils.contains(SUPPORTED_RECIPES, result)) {
      if (event.isShiftClick()) {
        if (CropUtils.getMultiplierPerm((Player) player) != null) {
          int least = getLeast(craftInventory.getMatrix());
          subtract(craftInventory.getMatrix(), least);

          boolean didMulti = multiplyResult((Player) player, least, result);
          if (didMulti) {
            ((Player) player).playSound(
                player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
          }
          craftInventory.setResult(null);
          event.setCancelled(true);
        }
      }
    }
  }

  @EventHandler
  public void onSmelt(InventoryClickEvent event) {
    if (event.getInventory().getType() != InventoryType.FURNACE) {
      return;
    }
    if (event.getSlotType() != SlotType.RESULT) {
      return;
    }

    FurnaceInventory smeltInventory = (FurnaceInventory) event.getInventory();
    ItemStack result = smeltInventory.getResult();
    HumanEntity player = event.getWhoClicked();
    if (result == null || !(player instanceof Player)) {
      return;
    }

    if (CropUtils.contains(SUPPORTED_SMELTS, result)) {
      if (event.isShiftClick()) {
        if (CropUtils.getMultiplierPerm((Player) player) != null) {
          int amount = result.getAmount();
          result.setAmount(1);
          boolean didMulti = multiplyResult((Player) player, amount, result);

          if (didMulti) {
            ((Player) player).playSound(
                player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
          }
          smeltInventory.setResult(null);
          event.setCancelled(true);
        }
      }
    }
  }

  private boolean multiplyResult(Player player, int amount, ItemStack result) {
    boolean didMulti = false;
    for (int i = 0; i < amount; i++) {
      int[] multi_chance = CropUtils.getMultiplier(player, config);
      boolean doMulti = (new Random().nextInt(100) + 1) <= multi_chance[1];
      int amt = doMulti ? multi_chance[0] : 1;
      if (amt != 1) {
        didMulti = true;
      }

      for (int j = 0; j < amt; j++) {
        if (hasEmptySlots(player.getInventory()) || hasFreeResultSlot(player.getInventory(),
            result)) {
          player.getInventory().addItem(result);
        } else {
          player.getWorld().dropItem(player.getLocation(), result);
        }
      }
    }
    return didMulti;
  }

  private int getLeast(ItemStack[] matrix) {
    int least = 100;
    for (ItemStack s : matrix) {
      if (s == null) {
        continue;
      }
      least = (s.getAmount() < least) ? s.getAmount() : least;
    }
    return least;
  }

  private void subtract(ItemStack[] matrix, int amt) {
    for (ItemStack s : matrix) {
      if (s == null) {
        continue;
      }
      s.setAmount(s.getAmount() - amt);
    }
  }

  private boolean hasFreeResultSlot(PlayerInventory inv, ItemStack result) {
    for (ItemStack s : inv.getStorageContents()) {
      if (s == null) {
        continue;
      }
      if (s.isSimilar(result) && s.getAmount() <= 64 - result.getAmount()) {
        return true;
      }
    }
    return false;
  }

  private boolean hasEmptySlots(PlayerInventory inv) {
    return Arrays.asList(inv.getStorageContents()).contains(null);
  }

  private void placePretty(PlayerInventory inventory, ItemStack result) {

  }
}
