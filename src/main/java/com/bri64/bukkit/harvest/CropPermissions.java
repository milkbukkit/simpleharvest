package com.bri64.bukkit.harvest;

public enum CropPermissions {

  USE("simpleharvest.use"), BONE("simpleharvest.bone"), MULTI("simpleharvest.multi.");

  private String perm;

  CropPermissions(String s) {
    this.perm = s;
  }

  @Override
  public String toString() {
    return perm;
  }
}
