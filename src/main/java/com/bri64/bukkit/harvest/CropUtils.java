package com.bri64.bukkit.harvest;

import com.bri64.bukkit.harvest.config.ConfigLoader;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.PermissionAttachmentInfo;

@SuppressWarnings("WeakerAccess")
public class CropUtils {

  public static FileConfiguration DEFAULT_CONFIG;

  static {
    DEFAULT_CONFIG = new YamlConfiguration();
    ConfigurationSection ch = DEFAULT_CONFIG.createSection("chances");
    ch.set("default", 50);
    ch.set("2", 75);
    ch.set("3", 50);
    ch.set("4", 25);
    ch.set("5", 10);
  }

  public static ItemStack createStack(Material material, short durability) {
    ItemStack s = new ItemStack(material);
    s.setDurability(durability);
    return s;
  }

  public static boolean contains(Collection<ItemStack> set, ItemStack s) {
    for (ItemStack i : set) {
      if (itemSimilar(i, s)) {
        return true;
      }
    }
    return false;
  }

  public static boolean itemSimilar(ItemStack stack1, ItemStack stack2) {
    return stack1 != null
        && stack2 != null
        && stack1.getType() == stack2.getType()
        && stack1.getDurability() == stack2.getDurability();
  }

  public static int[] getMultiplier(Player player, ConfigLoader config) {
    String multiPerm = getMultiplierPerm(player);
    if (multiPerm != null) {
      String[] perm = multiPerm.replace(CropPermissions.MULTI.toString(), "").split("\\.");

      int multi = 1;
      int chance = config.getChance(1);
      try {
        multi = Integer.parseInt(perm[0]);
      } catch (Exception ex) {
        return new int[]{multi, chance};
      }

      try {
        chance = Integer.parseInt(perm[1]);
      } catch (Exception ex) {
        return new int[]{multi, config.getChance(multi)};
      }

      if (multi >= 2) {
        if (chance >= 1 && chance <= 100) {
          return new int[]{multi, chance};
        }
        return new int[]{multi, config.getChance(multi)};
      }
      return new int[]{1, config.getChance(1)};
    }
    return new int[]{1, config.getChance(1)};
  }

  public static String getMultiplierPerm(Player player) {
    List<String> perms = player.getEffectivePermissions().stream().map(
        PermissionAttachmentInfo::getPermission).collect(Collectors.toList());
    for (String s : perms) {
      if (s.matches("^" + CropPermissions.MULTI.toString() + "*")) {
        return s;
      }
    }
    return null;
  }

  public static boolean holdingBone(ItemStack hand) {
    return hand != null
        && hand.getType().equals(Material.INK_SACK)
        && hand.getDurability() == 15;
  }

  public static <T> T listRand(List<T> list) {
    int index = new Random().nextInt(list.size());
    return list.get(index);
  }
}
