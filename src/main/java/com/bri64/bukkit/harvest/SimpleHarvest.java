package com.bri64.bukkit.harvest;

import static com.bri64.bukkit.harvest.CropUtils.DEFAULT_CONFIG;

import com.bri64.bukkit.harvest.config.ConfigLoader;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import java.io.IOException;
import java.util.logging.Logger;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

@SuppressWarnings({"unused", "WeakerAccess"})
public class SimpleHarvest extends JavaPlugin {

  public static SimpleHarvest PLUGIN;
  private Logger log;

  public Logger getLog() {
    return log;
  }

  private Plugin worldGuard;

  public boolean hasWorldGuard() {
    return worldGuard != null;
  }

  public WorldGuardPlugin getWorldGuard() {
    return (hasWorldGuard()) ? (WorldGuardPlugin) worldGuard : null;
  }

  public SimpleHarvest() {
    PLUGIN = this;
    this.log = getLogger();
    this.worldGuard = null;

    if (getConfig().getKeys(false).size() == 0) {
      try {
        log.info("No config.yml found, creating default...");
        DEFAULT_CONFIG.save(getDataFolder().getPath() + "/config.yml");
        reloadConfig();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  public void onDisable() {
    log.info("Disabled!");
  }

  @Override
  public void onEnable() {
    this.worldGuard = getServer().getPluginManager().getPlugin("WorldGuard");

    ConfigLoader config = new ConfigLoader(this, getConfig());

    getServer().getPluginManager().registerEvents(new CropListener(this, config), this);
    getServer().getPluginManager().registerEvents(new CraftListener(this, config), this);
    log.info("Enabled!");
  }


}
