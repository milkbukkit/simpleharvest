package com.bri64.bukkit.harvest.crops;

import static com.bri64.bukkit.harvest.CropUtils.listRand;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.bukkit.CropState;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Crops;

@SuppressWarnings("WeakerAccess")
public class BasicCrop extends Crop {

  private static Map<Material, ItemStack> BASIC_CROPS = new HashMap<>();

  static {
    BASIC_CROPS.put(Material.CROPS, new ItemStack(Material.SEEDS));
    BASIC_CROPS.put(Material.POTATO, new ItemStack(Material.POTATO_ITEM));
    BASIC_CROPS.put(Material.CARROT, new ItemStack(Material.CARROT_ITEM));
    BASIC_CROPS.put(Material.BEETROOT_BLOCK, new ItemStack(Material.BEETROOT_SEEDS));
  }

  public BasicCrop(final Block block) {
    super(block, BASIC_CROPS.get(block.getType()));
  }

  @Override
  public void grow() {
    CropState state = ((Crops) this.data).getState();
    List<CropState> possible = Arrays.stream(CropState.values())
        .filter((s) -> s.ordinal() > state.ordinal()).collect(Collectors.toList());
    ((Crops) this.data).setState(listRand(possible));
  }

  @Override
  public boolean isRipe() {
    return ((Crops) this.data).getState().equals(CropState.RIPE);
  }

  @Override
  public void setSeeded() {
    ((Crops) this.data).setState(CropState.SEEDED);
  }

  @Override
  public void setRipe() {
    ((Crops) this.data).setState(CropState.RIPE);
  }

}
