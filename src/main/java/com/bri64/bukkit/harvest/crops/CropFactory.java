package com.bri64.bukkit.harvest.crops;

import org.bukkit.block.Block;

public class CropFactory {

  public static Crop createCrop(Block block) {
    switch (block.getType()) {
      // Basic Crops
      case CROPS:
      case POTATO:
      case CARROT:
      case BEETROOT_BLOCK:
        return new BasicCrop(block);
      // Cocoa Crops
      case COCOA:
        return new CocoaCrop(block);
      // Nether Crops
      case NETHER_WARTS:
        return new NetherCrop(block);
      // Seedless Crops
      case HUGE_MUSHROOM_1:
      case HUGE_MUSHROOM_2:
        return new SeedlessCrop(block);
      default:
        return null;
    }
  }

  public static boolean isSupported(Block block) {
    return CropFactory.createCrop(block) != null;
  }
}
