package com.bri64.bukkit.harvest.crops;

import static com.bri64.bukkit.harvest.CropUtils.listRand;

import com.bri64.bukkit.harvest.CropUtils;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.material.CocoaPlant;
import org.bukkit.material.CocoaPlant.CocoaPlantSize;

@SuppressWarnings("WeakerAccess")
public class CocoaCrop extends Crop {

  private static short COCOA_BEAN_ID = 3;

  public CocoaCrop(final Block block) {
    super(block, CropUtils.createStack(Material.INK_SACK, COCOA_BEAN_ID));
  }

  @Override
  public void grow() {
    CocoaPlantSize state = ((CocoaPlant) this.data).getSize();
    List<CocoaPlantSize> possible = Arrays.stream(CocoaPlantSize.values())
        .filter((s) -> s.ordinal() > state.ordinal()).collect(Collectors.toList());
    ((CocoaPlant) this.data).setSize(listRand(possible));
  }

  @Override
  public boolean isRipe() {
    return ((CocoaPlant) this.data).getSize().equals(CocoaPlantSize.LARGE);
  }

  @Override
  public void setSeeded() {
    ((CocoaPlant) this.data).setSize(CocoaPlantSize.SMALL);
  }

  @Override
  public void setRipe() {
    ((CocoaPlant) this.data).setSize(CocoaPlantSize.LARGE);
  }
}
