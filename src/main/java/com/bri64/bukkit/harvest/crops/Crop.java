package com.bri64.bukkit.harvest.crops;

import com.bri64.bukkit.harvest.CropUtils;
import java.util.Arrays;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;

@SuppressWarnings("WeakerAccess")
public abstract class Crop implements GrowableCrop {

  protected Block block;
  protected Material blockType;
  protected ItemStack seed;
  protected BlockState state;
  protected MaterialData data;

  public Crop(final Block block, final ItemStack seed) {
    this.block = block;
    this.blockType = block.getType();
    this.state = block.getState();
    this.data = state.getData();
    this.seed = seed;
  }

  public void doBreak(Player player, int amt) {
    for (int i = 0; i < amt - 1; i++) {
      fakeBreak();
      setRipe();
      update();
    }

    fakeBreak();
    if (hasSeed(player)) {
      takeSeed(player);
      setSeeded();
    } else {
      clear();
    }
    update();
  }

  public void fakeBreak() {
    Material prev = block.getType();
    block.breakNaturally();
    block.setType(prev);
  }

  public void clear() {
    block.setType(Material.AIR);
  }

  public void update() {
    state.setData(data);
    state.update();
  }

  public boolean hasSeed(Player player) {
    if (player.getGameMode() == GameMode.CREATIVE) {
      return true;
    }
    return CropUtils.contains(Arrays.asList(player.getInventory().getStorageContents()), seed);
  }

  public void takeSeed(Player player) {
    if (player.getGameMode() != GameMode.CREATIVE) {
      for (ItemStack stack : player.getInventory().getContents()) {
        if (CropUtils.itemSimilar(stack, seed)) {
          stack.setAmount(stack.getAmount() - 1);
          break;
        }
      }
    }
  }

  @Override
  public boolean isLeftClickable() {
    return true;
  }

  @Override
  public boolean isRightClickable() {
    return true;
  }

  @Override
  public void grow() {
  }

  @Override
  public boolean isRipe() {
    return false;
  }

  @Override
  public void setSeeded() {
  }

  @Override
  public void setRipe() {
  }

  @Override
  public void animateHarvest() {
    block.getWorld().spawnParticle(
        Particle.SPELL_WITCH,
        block.getLocation().add(0.5, 1.0, 0.5),
        30,
        0.25, 0, 0.25,
        20);
  }

  @Override
  public void animateGrowth() {
  }
}
