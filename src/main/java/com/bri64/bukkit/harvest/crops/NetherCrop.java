package com.bri64.bukkit.harvest.crops;

import static com.bri64.bukkit.harvest.CropUtils.listRand;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.bukkit.Material;
import org.bukkit.NetherWartsState;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.NetherWarts;

@SuppressWarnings("WeakerAccess")
public class NetherCrop extends Crop {

  public NetherCrop(final Block block) {
    super(block, new ItemStack(Material.NETHER_STALK));
  }

  @Override
  public boolean isRipe() {
    return ((NetherWarts) this.data).getState().equals(NetherWartsState.RIPE);
  }

  @Override
  public void setSeeded() {
    ((NetherWarts) this.data).setState(NetherWartsState.SEEDED);
  }

  @Override
  public void setRipe() {
    ((NetherWarts) this.data).setState(NetherWartsState.RIPE);
  }

  public void grow() {
    NetherWartsState state = ((NetherWarts) this.data).getState();
    List<NetherWartsState> possible = Arrays.stream(NetherWartsState.values())
        .filter((s) -> s.ordinal() > state.ordinal()).collect(Collectors.toList());
    ((NetherWarts) this.data).setState(listRand(possible));
  }

  @Override
  public void animateGrowth() {
    block.getWorld().spawnParticle(
        Particle.VILLAGER_HAPPY,
        block.getLocation().add(0.6, 0.3, 0.6),
        15,
        0.25, 0.2, 0.25,
        40);
  }
}
