package com.bri64.bukkit.harvest.crops;

public interface GrowableCrop {

  boolean isLeftClickable();

  boolean isRightClickable();

  void grow();

  boolean isRipe();

  void setSeeded();

  void setRipe();

  void animateHarvest();

  void animateGrowth();

}
