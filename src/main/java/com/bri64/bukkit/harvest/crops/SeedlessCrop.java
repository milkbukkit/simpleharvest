package com.bri64.bukkit.harvest.crops;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;

@SuppressWarnings("WeakerAccess")
public class SeedlessCrop extends Crop {

  public SeedlessCrop(Block block) {
    super(block, null);
  }

  @Override
  public boolean isRightClickable() {
    return false;
  }

  @Override
  public boolean isRipe() {
    return true;
  }

  @Override
  public void setRipe() {
    block.setType(blockType);
  }

  @Override
  public boolean hasSeed(Player player) {
    return false;
  }

  @Override
  public void takeSeed(Player player) {
  }
}
